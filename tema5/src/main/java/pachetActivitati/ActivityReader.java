package pachetActivitati;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ActivityReader {

	private final String fileName = "Activities.txt";
	private List<MonitoredData> listaDate;
	private final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public ActivityReader() {
		Stream<String> stream = null;
		try {
			stream = Files.lines(Paths.get(fileName));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		this.setListaDate(stream.map(temp -> {
			String[] campuri = temp.split("\\s{2,}");
			campuri[0] = campuri[0].trim();
			campuri[1] = campuri[1].trim();
			campuri[2] = campuri[2].trim();

			LocalDateTime start, end;
			start = LocalDateTime.parse(campuri[0], format); 
			end = LocalDateTime.parse(campuri[1], format);
			MonitoredData aux = new MonitoredData(start, end, campuri[2]);
			return aux;
		}).collect(Collectors.toList()));
		
	}

	public List<MonitoredData> getListaDate() {
		return listaDate;
	}

	public void setListaDate(List<MonitoredData> listaDate) {
		this.listaDate = listaDate;
	}

}
