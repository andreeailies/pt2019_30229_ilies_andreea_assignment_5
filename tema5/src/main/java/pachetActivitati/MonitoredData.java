package pachetActivitati;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MonitoredData {

	public LocalDateTime startTime;
	public LocalDateTime endTime;
	public String activityLabel;
	
	private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	public MonitoredData(LocalDateTime start, LocalDateTime end, String label) {
		this.startTime = start;
		this.endTime = end;
		this.activityLabel = label;
	}

	public LocalDateTime getStartData() {
		LocalDateTime aux = startTime.minusHours(startTime.getHour());
		 aux = aux.minusMinutes(aux.getMinute());
		 aux = aux.minusSeconds(aux.getSecond());
		 return aux;
	}
	
	public LocalDateTime getEndData() {
		LocalDateTime aux = endTime.minusHours(endTime.getHour());
		 aux = aux.minusMinutes(aux.getMinute());
		 aux = aux.minusSeconds(aux.getSecond());
		 return aux;
	}

	/**
	 * @return the startTime
	 */
	public LocalDateTime getStartTime() {
		return startTime;
	}



	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(LocalDateTime startTime) {
		this.startTime = startTime;
	}



	/**
	 * @return the endTime
	 */
	public LocalDateTime getEndTime() {
		return endTime;
	}



	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(LocalDateTime endTime) {
		this.endTime = endTime;
	}



	/**
	 * @return the activityLabel
	 */
	public String getActivityLabel() {
		return activityLabel;
	}



	/**
	 * @param activityLabel the activityLabel to set
	 */
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}

	public String toString() {
		return startTime.format(format) + "    " +
				endTime.format(format);
	}

}
