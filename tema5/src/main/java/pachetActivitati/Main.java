package pachetActivitati;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public class Main {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// citesc activitatiile din fisier si le pun in lista
		ActivityReader citire = new ActivityReader();
		
		// cerinte:
		Cerinte cerinte = new Cerinte(citire.getListaDate());
		
		// C1 numarul de zile cu activitate
		int nrZile = cerinte.numarZilele();
		System.out.println("C1: Numarul de zile unice in care s-a desfasurat activitatea este: " + nrZile);
		
		// C2 frecventa fiecarei activitatii
		Map<String, Integer> frecventa = cerinte.frecventaActivitati();
		CerinteWriter.afisareMapSI(true, frecventa);
		
		// C3 frecventa activitatilor penru fiecare zi in parte
		Map<String, Map<LocalDateTime, Integer>> frecvZile = cerinte.frecventaPeZile();
		CerinteWriter.afisareMapComplexa(true, frecvZile);
		
		// C4 durata activitatilor pentru fiecare linie a sa
		Map<String, Map<MonitoredData,Integer>> durataLinii = cerinte.durataLinie();
		CerinteWriter.afisareMapComplexa(false, durataLinii);
		
		// C5 durata totala a fiecarei activitati
		Map<String, Integer> total = cerinte.durataTotalaActivitati();
		CerinteWriter.afisareMapSI(false, total);
		
		// C6 activitati cu inregistrari de sub 5 min ce depasesc 90%
		List<String> scurte = cerinte.sub5Min();
		CerinteWriter.afisareLista(scurte);
		
	}

}
