package pachetActivitati;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

public class CerinteWriter {

	public static void afisareMapSI(boolean cerinta, Map<String, Integer> mapa) {
		FileWriter write = null;
		PrintWriter printer = null;
		String mesaj = null;
		try {
			if (cerinta) {
				write = new FileWriter("frecventaActivitati.txt", false);
				mesaj = "Numele si frecventa activitatii:";
			} else {
				write = new FileWriter("durataActivitati.txt", false);
				mesaj = "Numele si durata activitatii (in secunde):";
			}
			printer = new PrintWriter(write);
			printer.println(mesaj + "\n");
			
			for(String str : mapa.keySet()) {
				printer.println(str + ": " + mapa.get(str));
			}
			
		} catch (Exception eee) {
			JOptionPane.showMessageDialog(null, "Nu am putut scrie in document!", "Failed!", JOptionPane.ERROR_MESSAGE);
		} finally {
			if (printer != null)
				printer.close();
		}
	}

	public static void afisareLista(List<String> lista) {
		FileWriter write = null;
		PrintWriter printer = null;;
		try {
				write = new FileWriter("peste90.txt", false);
			printer = new PrintWriter(write);
			printer.println("Activitatiile cu durata sub 5 minute peste 90%:");
			
			for(String str : lista) {
				printer.println(str);
			}
			
		} catch (Exception eee) {
			JOptionPane.showMessageDialog(null, "Nu am putut scrie in document!", "Failed!", JOptionPane.ERROR_MESSAGE);
		} finally {
			if (printer != null)
				printer.close();
		}
	}
	
	private static final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	
	public static <T> void afisareMapComplexa(boolean cerinta, Map<String, Map< T, Integer>> mapa) {
		FileWriter write = null;
		PrintWriter printer = null;
		String mesaj = null;
		try {
			if (cerinta) {
				write = new FileWriter("frecventaPeZile.txt", false);
				mesaj = "Frecventa unei activitati intr-o zi specifica:";
			} else {
				write = new FileWriter("durataPeLinii.txt", false);
				mesaj = "Durata unei activitati de pe o anumita linie (in secunde)";
			}
			printer = new PrintWriter(write);
			printer.println(mesaj + "\n");
			
			for(String str : mapa.keySet()) {

				for (Object ob: mapa.get(str).keySet()) {
					String val;
					if (cerinta == false) {
						val = ((MonitoredData)ob).toString();
					} else {
						val = ((LocalDateTime)ob).format(format);
					}
					printer.println(str + " pentru " + val + ": " + mapa.get(str).get(ob));
				}
			}
			
		} catch (Exception eee) {
			JOptionPane.showMessageDialog(null, "Nu am putut scrie in document!", "Failed!", JOptionPane.ERROR_MESSAGE);
		} finally {
			if (printer != null)
				printer.close();
		}
	}
}
