package pachetActivitati;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class Cerinte {

	private List<MonitoredData> lista;

	public Cerinte(List<MonitoredData> lista) {
		this.lista = lista;
	}

	public int numarZilele() {
		int numar = lista.stream().reduce((prev, next) -> {
			if (prev == null) return next;
			if (next == null) return prev;
			if (prev.getEndData().equals(next.getStartData()) == true) { 
				return new MonitoredData(prev.getStartData(), next.getEndData(), " ");
			} else { 
				return new MonitoredData(prev.getStartData(), 
						next.getEndData().minusDays(ChronoUnit.DAYS.between(next.getStartData(), prev.getEndData())), " ");
			}
		}).map(a -> (int) ChronoUnit.DAYS.between(a.getStartData(), a.getEndData())).get() + 1;
		
		return numar;
	}
	
	public Map<String, Integer> frecventaActivitati() {
		return lista.stream().collect(
				Collectors.toMap(MonitoredData::getActivityLabel, counter -> 1, Integer::sum, LinkedHashMap::new));
	}
	
	public Map<String, Map<LocalDateTime, Integer>> frecventaPeZile() {
		return lista.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel,
				Collectors.toMap(MonitoredData::getStartData, counter -> 1, Integer::sum, LinkedHashMap::new)));
	}
	
	public Map<String, Map<MonitoredData, Integer>> durataLinie() {
		return lista.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivityLabel,
						Collectors.toMap(linie -> linie,
								counter -> (int) ChronoUnit.SECONDS.between(counter.startTime, counter.endTime),
								Integer::sum, LinkedHashMap::new)));
	}
	
	public Map<String, Integer> durataTotalaActivitati() {
		return lista.stream()
				.collect(Collectors.toMap(MonitoredData::getActivityLabel,
						counter -> (int) ChronoUnit.SECONDS.between(counter.startTime, counter.endTime), Integer::sum,
						LinkedHashMap::new));
	}
	
	public List<String> sub5Min() {
		Map<String, Integer> fcv = frecventaActivitati();
		return lista.stream()
				.collect(Collectors.toMap(MonitoredData::getActivityLabel,  counter -> (ChronoUnit.SECONDS.between(counter.startTime, counter.endTime) < 300 ? 1 : 0),
						Integer::sum, LinkedHashMap::new)).entrySet().stream()
				.filter(a -> ((100 * (float)a.getValue()) / fcv.get(a.getKey()) >= 90))
				.map(a -> a.getKey())
				.collect(Collectors.toList());
	}
}
